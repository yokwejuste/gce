import React, {useState, useEffect} from 'react';
import axios from 'axios';


const App = () => {

    const [data, setData] = useState({});
    const [opt, setOpt] = useState("");
    const [student_name, setStudent_name] = useState("");
    const [year, setYear] = useState("");
    const [level, setLevel] = useState("");

    useEffect(() => {
        const postData = async () => {
            if (opt && student_name && year && level !== "") {
                axios({
                    method: 'post',
                    url: '/fetch-result.php',
                    data: {
                        "opt": opt,
                        "student_name": student_name,
                        "year": year,
                        "level": level
                    },
                    headers: {
                        "Content-Type": "multipart/form-data",
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Methods": "GET, POST, OPTIONS, PUT, PATCH, DELETE",
                        "Access-Control-Allow-Headers": "X-Requested-With,content-type",
                    },
                }).then((response) => {
                        setData(response.data);
                    }, (error) => {
                        console.log(error);
                    }
                );
            }
        }

        postData().then(r => console.log(r));
    }, [opt, student_name, year, level])

    return (
        <div>
            <form>
                <label>opt</label>
                <input type="text" name="opt" onChange={
                    (e) => setOpt(e.target.value)
                }/>
                <label>student_name</label>
                <input type="text" name="student_name" onChange={
                    (e) => setStudent_name(e.target.value)
                }/>
                <label>year</label>
                <input type="text" name="year" onChange={
                    (e) => setYear(e.target.value)
                }/>
                <label>level</label>
                <input type="text" name="level" onChange={
                    (e) => setLevel(e.target.value)
                }/>
            </form>
            <table border={2}>
                <thead>
                <tr>
                    <th width={5}>S/N</th>
                    <th width={500} align={"left"}>Student Name</th>
                    <th width={500} align={"left"}>Center Name</th>
                    <th width={500} align={"left"}>Center Number</th>
                    <th width={10} align={"left"}>Passed Papers</th>
                    <th width={300} align={"left"}>Student Grades</th>
                </tr>
                </thead>
                <tbody>
                {data["data"] && data["data"].map((item, index) => (
                    <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{item["student_name"]}</td>
                        <td>{item["center_name"]}</td>
                        <td>{item["center_number"]}</td>
                        <td>{item["papers_passed"]}</td>
                        <td>{item["student_grades"]}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>
    );
}

export default App;