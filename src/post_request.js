import React, {useState, useEffect} from 'react';
import axios from 'axios';

function PostRequest() {
    const [data, setData] = useState({});

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.post('https://jean.tdev-hub.space/fetch-result.php', {
                "opt": "7",
                "student_name": "agahnui",
                "year": "2020",
                "level": "ALG"
            });
            setData(result.data);
        };
        fetchData().then(r => console.log(r));
    }, []);

    return (
        <div>
            <p>POST Request</p>
            <pre>{JSON.stringify(data, null, 2)}</pre>
        </div>
    );
}

export default PostRequest;