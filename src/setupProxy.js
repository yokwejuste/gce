const {createProxyMiddleware} = require('http-proxy-middleware');

module.exports = app => {
    app.use(
        createProxyMiddleware(
            '/fetch-result.php', {
                target: 'https://jean.tdev-hub.space',
                changeOrigin: true,
            })
    );
}
