import React from "react";
import axios from "axios";
// a get request to the api

const App = () => {
    const [data, setData] = React.useState(null);
    const [loading, setLoading] = React.useState(false);
    const [error, setError] = React.useState(null);

    const fetchData = async () => {
        try {
            // set error and loading to null
            setError(null);
            setLoading(true);
            // make request to api
            const response = await axios.get(
                "https://api.github.com/users/QuincyLarson"
            );
            // set data to response
            setData(response.data);
        } catch (error) {
            // set error to error
            setError(error);
        }
        // set loading to false
        setLoading(false);
    };

    React.useEffect(() => {
        fetchData().then(r => console.log(r));
    }, []);

    // if loading is true
    if (loading) return <div>Loading...</div>;
    // if error is true
    if (error) return <div>Error!</div>;
    // if data is null
    if (!data) return <div>No data</div>;

    return (
        <div>
            <h1>{data.name}</h1>
            <p>{data.location}</p>
        </div>
    );
}


export default App;
